# simple-login

A simple login/signup project made for practice of [jQuery.ajax()](http://api.jquery.com/jquery.ajax/) and RESTful API.

Technologies used - 
- [Express](https://expressjs.com/) for making a basic API
- [mlab](https://mlab.com/welcome/) as host of [MongoDB](https://www.mongodb.com/) database which stores username and password of all users that sign up
- [jQuery.ajax()](http://api.jquery.com/jquery.ajax/) API for making asynchronous requests to the server

**NOTE:-**
- Project currently runs only on a local server provided by [Express](https://expressjs.com/)
- UI was not given much importance as the aim was only to get basic understanding of how Frontend and Backend interact and work together.

![ss1](https://raw.githubusercontent.com/kunal-mohta/simple-login/master/screenshots/ss1.png)
![ss2](https://raw.githubusercontent.com/kunal-mohta/simple-login/master/screenshots/ss2.png)
![ss3](https://raw.githubusercontent.com/kunal-mohta/simple-login/master/screenshots/ss3.png)
![ss4](https://raw.githubusercontent.com/kunal-mohta/simple-login/master/screenshots/ss4.png)
